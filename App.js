import React from "react";
import { StyleSheet, View, ActivityIndicator, Text } from "react-native";
import { WebView } from "react-native-webview";
import Constants from "expo-constants";
export default function App() {
  return (
    <View
      style={{
        flex: 1,
        paddingTop: Constants.statusBarHeight,
        position: "relative",
      }}>
      <WebView
        source={{ uri: "http://192.168.11.60:9900/" }}
        startInLoadingState
        renderLoading={() => (
          <View style={styles.loading}>
            <ActivityIndicator
              size="large"
              color="#1f65b3"
            />
          </View>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  loading: {
    position: "absolute",
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  }
})
